<?php
namespace Mirror\MirrorSiteBundle\Tests\Model;

use Mirror\MirrorSiteBundle\Model\TNavi;

/**
 * Class TNaviTest
 * @package Mirror\MirrorSiteBundle\Tests\Model
 */
class TNaviTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @param $count
     * @param $perPage
     * @dataProvider NavigationTestProvider
     */
    public function testNavigationForAllPageNumber($count, $perPage)
    {
        $navigation = new TNavi($count, $perPage, 0);
        $maxFromObject = $navigation->getMax();
        $trueNotFoundFlag = false;

        $this->assertEquals($trueNotFoundFlag, $navigation->pageNotExist());

        $trueFirstResult = 0;
        for($i=$maxFromObject + 1; $i>0; $i--) {
            $navigation = new TNavi($count, $perPage, $i);
            $trueNotFoundFlag = ($i == $maxFromObject + 1);
            $this->assertEquals($trueNotFoundFlag, $navigation->pageNotExist(), "Invalid not-found-flag for page $i");

            if(!$navigation->pageNotExist()) {
                $this->assertEquals($trueFirstResult, $navigation->getFirstResult(), "Invalid first result for page $i");
                $trueFirstResult += $perPage;
            }

        }

    }

    public function NavigationTestProvider()
    {
        return [
            [0, 3],
            [1, 3],
            [17, 3],
            [26, 4],
            [151, 5],
            [-1, -4]
        ];
    }

}
