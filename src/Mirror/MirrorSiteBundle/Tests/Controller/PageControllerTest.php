<?php

namespace Mirror\MirrorSiteBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PageControllerTest extends WebTestCase
{
    /**
     * @param $route
     *
     * @dataProvider notLoggedInProvider
     */
    public function testNotLoggedIn($route)
    {
        $client = static::createClient();

        $crawler = $client->request('GET', $route);

        $headerMenu = $crawler->filter('body > header > nav');
        $this->assertEquals( 1, $headerMenu->filter('form')->count() );

        for($i=0; $i<3; $i++) {
            $homeCrawler = $headerMenu->filter('a')->eq($i)->link();
            $this->assertEquals( 'GET', $homeCrawler->getMethod() );
        }
    }

    public function notLoggedInProvider()
    {
        return [
          ['/'], ['/top'], ['trash']
        ];
    }

}
