<?php
namespace Mirror\MirrorSiteBundle\Service;

use Doctrine\ORM\EntityManager;
use Mirror\MirrorSiteBundle\Entity\User;
use Mirror\MirrorSiteBundle\Entity\Vote;
use Mirror\MirrorSiteBundle\Entity\Attachment;

/**
 * Class VoteService
 * @package Mirror\MirrorSiteBundle\Service
 */
class VoteService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Attachment
     */
    private $file;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param Vote $vote
     * @return bool
     */
    public function delete(Vote $vote)
    {
        $this->file = $this->em->getRepository("MirrorMirrorSiteBundle:Attachment")->find($vote->getFile());
        if($this->file === null) return false;
        $this->file->voting($vote->getValue() * -1);
        $this->em->remove($vote);
        $this->em->persist($this->file);
        $this->em->flush();
        return true;
    }

    /**
     * @param Vote $vote
     * @return bool
     */
    public function add(Vote $vote)
    {
        $this->file = $this->em->getRepository("MirrorMirrorSiteBundle:Attachment")->find($vote->getFile());
        if($this->file === null) return false;
        $this->file->voting($vote->getValue());
        $this->em->persist($this->file);
        $vote->setWhen(new \DateTime());
        $this->em->persist($vote);
        $this->em->flush();
        return true;
    }

    /**
     * @param Vote $vote
     * @return bool
     */
    public function tog(Vote $vote)
    {
        $this->delete($vote);
        $value = $vote->getValue();
        $vote->setValue($value * -1);
        return $this->add($vote);
    }

    /**
     * @param User $user
     * @return array
     */
    public function getColors(User $user)
    {
        $vote = $this->em->getRepository("MirrorMirrorSiteBundle:Vote")->findBy([
            'file' => $this->file->getId(),
            'user' => $user->getId()
        ]);

        $response = [];
        if(count($vote)) {
            if($vote[0]->getValue() == 1) {
                $response['up'] = '#20812e';
                $response['down'] = 'white';
            } else {
                $response['up'] = 'white';
                $response['down'] = '#20812e';
            }
        } else {
            $response['up'] = 'white';
            $response['down'] = 'white';
        }
        return $response;
    }

    /**
     * @return bool|Attachment
     */
    public function getFile()
    {
        return $this->file === null ? false : $this->file;
    }

}