<?php
namespace Mirror\MirrorSiteBundle\Exception;

/**
 * Class InvalidEntityException
 * @package Mirror\MirrorSiteBundle\Exception
 */
class InvalidEntityException extends \Exception
{
    /**
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct($code = 0, \Exception $previous = null)
    {
        parent::__construct("Invalid entity. Required instance of Attachment.", $code, $previous);
    }

    public function __toString()
    {
        return "Invalid entity. Required instance of Attachment.";
    }
}
