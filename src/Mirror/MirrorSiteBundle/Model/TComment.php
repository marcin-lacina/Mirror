<?php
namespace Mirror\MirrorSiteBundle\Model;

use Mirror\MirrorSiteBundle\Entity\Comment;
use Mirror\MirrorSiteBundle\Entity\User;

/**
 * Class TComment
 * @package Mirror\MirrorSiteBundle\Model
 */
class TComment
{

    /**
     * @var User
     */
    private $user;

    /**
     * @var Comment
     */
    private $comment;

    /**
     * @param Comment $comment
     */
    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * @param User $user
     * @return User
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this->user;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return Comment
     */
    public function getComment()
    {
        return $this->comment;
    }
}