<?php
namespace Mirror\MirrorSiteBundle\Model;

/**
 * Class TOrderFile
 * @package Mirror\MirrorSiteBundle\Model
 */
class TOrderFile
{
    /**
     * @var string
     */
    private $by;

    /**
     * @var string
     */
    private $how;

    /**
     * @param $sort
     */
    public function __construct($sort)
    {
        if($sort === null) {
            $this->by = 'f.when_';
            $this->how = 'desc';
        } else {
            $this->by = 'f.vote';
            $this->how = $sort == 'top' ? 'desc' : 'asc';
        }
    }

    /**
     * @return string
     */
    public function getBy()
    {
        return $this->by;
    }


    /**
     * @return string
     */
    public function getHow()
    {
        return $this->how;
    }
}