<?php
namespace Mirror\MirrorSiteBundle\Model;

/**
 * Class TNavi
 * @package Mirror\MirrorSiteBundle\Model
 */
class TNavi
{
    /**
     * @var int
     */
    private $perPage;

    /**
     * @var int
     */
    private $max;

    /**
     * @var int
     */
    private $maxModulo;

    /**
     * @var int
     */
    private $min;

    /**
     * @var int
     */
    private $count;

    /**
     * @var int
     */
    private $current;

    /**
     * @param $filesCount
     * @param $perPage
     * @param $current
     */
    public function __construct($filesCount, $perPage, $current)
    {
        $this->perPage = (int)$perPage;
        $this->count = (int)$filesCount;
        $this->current = (int)$current;
        $this->max = (int)($filesCount/$perPage);
        $this->maxModulo = $filesCount % $perPage;
        if($this->maxModulo) $this->max++;
        if(!$this->current) $this->current = $this->max;
        if($this->max > 12) {
            if($this->current > $this->max-6) {
                $this->min = $this->max-6;
            } else if($this->current < 6) {
                $this->max = 6;
                $this->min = 1;
            }
            else {
                $this->min = $this->current - 3;
                $this->max = $this->current + 3;
            }
        } else if(!$this->max) {
            $this->min = 0;
        } else {
            $this->min = 1;
        }
    }

    /**
     * @return int
     */
    public function getFirstResult()
    {
        if($this->current != $this->max) {
            $absolute = abs($this->count - $this->current*$this->perPage);

            if($this->maxModulo) return $absolute + $this->perPage-$this->maxModulo;
            return $absolute;
        }
        return 0;
    }
    /**
     * @return bool
     */
    public function pageNotExist()
    {
        return ($this->current > $this->max);
    }
    /**
     * @return int
     */
    public function getPerPage()
    {
        return $this->perPage;
    }

    /**
     * @return int
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * @return int
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @return int
     */
    public function getCurrent()
    {
        return $this->current;
    }

}
