<?php
namespace Mirror\MirrorSiteBundle\Model;

use Mirror\MirrorSiteBundle\Entity\Attachment;

/**
 * Class TPost
 * @package Mirror\MirrorSiteBundle\Model
 */
class TPost
{
    /**
     * @var Attachment
     */
    private $file;

    /**
     * @var array
     */
    private $comments;

    /**
     * @var string
     */
    private $upVote;

    /**
     * @var string
     */
    private $downVote;

    /**
     * @param Attachment $file
     * @param array $comments
     * @param $vote
     */
    public function __construct(Attachment $file, array $comments, $vote = null)
    {
        $this->file = $file;
        $this->comments = $comments;
        if($vote !== null) {
            switch($vote) {
                case -1: $this->upVote = 'white'; $this->downVote = '#20812e'; break;
                case 1: $this->upVote = '#20812e'; $this->downVote = 'white'; break;
                default: $this->upVote = 'white'; $this->downVote = 'white'; break;
            }
        }
    }

    /**
     * @return Attachment
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return array
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @return string
     */
    public function getUpVote()
    {
        return $this->upVote;
    }

    /**
     * @return string
     */
    public function getDownVote()
    {
        return $this->downVote;
    }

}