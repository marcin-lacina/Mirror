<?php
namespace Mirror\MirrorSiteBundle\Controller;

use Mirror\MirrorSiteBundle\Entity\Attachment;
use Mirror\MirrorSiteBundle\Model\TComment;
use Mirror\MirrorSiteBundle\Model\TNavi;
use Mirror\MirrorSiteBundle\Model\TOrderFile;
use Mirror\MirrorSiteBundle\Model\TPost;
use Mirror\MirrorSiteBundle\Exception\InvalidEntityException;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PageController
 * @package Mirror\MirrorSiteBunde\Controller
 */
class PageController extends Controller
{
    /**
     * @param $currentPage
     * @param $perPage
     * @param null $sort
     * @return Response
     */
    private function createPage($currentPage, $perPage, $sort = null)
    {
        $authenticationUtils = $this->get('security.authentication_utils');
        $lastUserName = $authenticationUtils->getLastUsername();
        $authError = $authenticationUtils->getLastAuthenticationError();

        $em = $this->getDoctrine()->getManager();
        $filesCount = $em->getRepository("MirrorMirrorSiteBundle:Attachment")->count();
        $navigation = new TNavi($filesCount, $perPage, $currentPage);

        if( $navigation->pageNotExist() )
            throw $this->createNotFoundException();

        $order = new TOrderFile($sort);
        $files = $em->getRepository("MirrorMirrorSiteBundle:Attachment")
            ->createQueryBuilder('f')->orderBy( $order->getBy(), $order->getHow() )
            ->setFirstResult( $navigation->getFirstResult() )
            ->setMaxResults($perPage)->getQuery()->getResult();

        return $this->render('MirrorMirrorSiteBundle:Page:home.html.twig', [
            'PageTitle' => $sort === null ? "home" : $sort,
            'Posts' => $this->getArrayPosts($files),
            'Auth' => [ 'lastUserName' => $lastUserName, 'error' => $authError ],
            'Navi' => $navigation
        ]);
    }

    /**
     * @param array $entities
     * @return array
     * @throws \Exception
     */
    private function getArrayPosts(array $entities)
    {
        $em = $this->getDoctrine()->getManager();
        $posts = [];
        foreach($entities as $entity) {
            if(!$entity instanceof Attachment) throw new InvalidEntityException();

            $commentsEntities =  $em->getRepository("MirrorMirrorSiteBundle:Comment")
                ->findByFileIdOrderByWhen( $entity->getId() );

            $comments = [];
            foreach($commentsEntities as $entityComment) {
                $newComment = new TComment($entityComment);
                $newComment->setUser(
                    $em->getRepository("MirrorMirrorSiteBundle:User")->find($entityComment->getUserId())
                );
                $comments[] = $newComment;
            }

            $posts[] = new TPost($entity, $comments);
        }
        return $posts;
    }

    /**
     * @param $page int
     * @param $sort (top|trash)
     * @return Response
     */
    public function sortAction($page = 0, $sort)
    {
        if($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute("user_sort", ['sort' => $sort]);
        }
        return $this->createPage($page, 3, $sort);
    }

    /**
     * @param $page
     * @return Response
     */
    public function homeAction($page = 0)
    {
        if($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute("user_home");
        }
        return $this->createPage($page, 3);
    }

    /**
     * @param $fileName
     * @return Response
     */
    public function viewAction($fileName)
    {
        $authenticationUtils = $this->get('security.authentication_utils');
        $lastUserName = $authenticationUtils->getLastUsername();
        $authError = $authenticationUtils->getLastAuthenticationError();

        $em = $this->getDoctrine()->getManager();
        $file = $em->getRepository("MirrorMirrorSiteBundle:Attachment")->findOneByFileName($fileName);
        if(!$file) throw $this->createNotFoundException();

        $commentsEntities =  $em->getRepository("MirrorMirrorSiteBundle:Comment")
            ->findByFileIdOrderByWhen( $file->getId() );

        $comments = [];
        foreach($commentsEntities as $oneComment) {
            $newComment = new TComment($oneComment);
            $newComment->setUser(
                $em->getRepository("MirrorMirrorSiteBundle:User")->find($oneComment->getUserId())
            );
            $comments[] = $newComment;
        }


        return $this->render('MirrorMirrorSiteBundle:Page:view.html.twig', [
            'file' => $file,
            'comments' => $comments,
            'Auth' => [ 'lastUserName' => $lastUserName, 'error' => $authError ]
        ]);
    }
}
