<?php
namespace Mirror\MirrorSiteBundle\Controller;

use Mirror\MirrorSiteBundle\Entity\Comment;
use Mirror\MirrorSiteBundle\Entity\Vote;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AjaxUserController
 * @package Mirror\MirrorSiteBundle\Controller
 */
class AjaxUserController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function commentAction(Request $request)
    {
        if(!$request->isXmlHttpRequest())
            throw $this->createNotFoundException();
        $this->denyAccessUnlessGranted('ROLE_USER');

        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $fileID = $request->request->get('file');
        $message = $request->request->get('comment');
        $comment = new Comment();
        $comment->setFileId($fileID);
        $comment->setMessage($message);
        $comment->setUserId($user->getId());
        $comment->setWhen(new \DateTime());
        $comment->setIp(
            $this->container->get('request_stack')->getCurrentRequest()->getClientIp()
        );
        $em->persist($comment);
        $em->flush();

        return new JsonResponse([
            'userName' => $user->getUsername(),
            'when' => $comment->getWhen()->format('d.m.Y H:i'),
            'id' => $fileID
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function voteAction(Request $request)
    {
        if(!$request->isXmlHttpRequest()) {
            throw $this->createNotFoundException();
        }
        $this->denyAccessUnlessGranted('ROLE_USER');

        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $fileID = $request->request->get('file');
        $vote = $request->request->get('vote');
        if( !in_array($vote, [1, -1]) ) throw $this->createAccessDeniedException();

        try {
            $exist = $em->getRepository("MirrorMirrorSiteBundle:Vote")->createQueryBuilder('v')
                ->where('v.user = :user AND v.file = :file')->setParameter('user', $user->getId())
                ->setParameter('file', $fileID)->getQuery()->getSingleResult();
        } catch(\Exception $e)
        {
            $exist = null;
        }


        $voteService = $this->get('mirror_mirror_site.voteService');

        if($exist !== null) {

            if($vote == $exist->getValue()) {

                if(!$voteService->delete($exist)) {
                    throw $this->createNotFoundException();
                }
            } else {
                if(!$voteService->tog($exist)) {
                    throw $this->createNotFoundException();
                }
            }

        } else {
            $newVote = new Vote();
            $newVote->setFile($fileID);
            $newVote->setUser($user->getId());
            $newVote->setValue($vote);

            if(!$voteService->add($newVote)) {
                throw $this->createNotFoundException();
            }

        }

        return new JsonResponse([
            'fileVote' => $voteService->getFile()->getId(),
            'value' => $voteService->getFile()->getVote(),
            'colors' => $voteService->getColors($user)
        ]);
    }
}
