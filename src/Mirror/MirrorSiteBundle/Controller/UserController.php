<?php
namespace Mirror\MirrorSiteBundle\Controller;

use Mirror\MirrorSiteBundle\Entity\Attachment;
use Mirror\MirrorSiteBundle\Model\TComment;
use Mirror\MirrorSiteBundle\Model\TNavi;
use Mirror\MirrorSiteBundle\Model\TOrderFile;
use Mirror\MirrorSiteBundle\Model\TPost;
use Mirror\MirrorSiteBundle\Exception\InvalidEntityException;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class PageController
 * @package Mirror\MirrorSiteBunde\Controller
 */
class UserController extends Controller
{
    /**
     * @param $currentPage
     * @param $perPage
     * @param null $sort
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    private function createPage($currentPage, $perPage, $sort = null)
    {
        $this->denyAccessUnlessGranted("ROLE_USER", null, "Access denied.");

        $em = $this->getDoctrine()->getManager();
        $filesCount = $em->getRepository("MirrorMirrorSiteBundle:Attachment")->count();
        $navigation = new TNavi($filesCount, $perPage, $currentPage);
        if( $navigation->pageNotExist() )
            throw $this->createNotFoundException();

        $order = new TOrderFile($sort);
        $files = $em->getRepository("MirrorMirrorSiteBundle:Attachment")
            ->createQueryBuilder('f')->orderBy( $order->getBy(), $order->getHow() )
            ->setFirstResult( $navigation->getFirstResult() )
            ->setMaxResults($perPage)->getQuery()->getResult();


        return $this->render('MirrorMirrorSiteBundle:User:home.html.twig', [
            'PageTitle' => $sort === null ? "home" : $sort,
            'Posts' => $this->getArrayPosts($files),
            'Navi' => $navigation,
            'User' => $this->getUser()
        ]);
    }

    /**
     * @param array $entities
     * @return array
     * @throws \Exception
     */
    private function getArrayPosts(array $entities)
    {
        $em = $this->getDoctrine()->getManager();
        $posts = [];
        foreach($entities as $entity) {
            if(!$entity instanceof Attachment) throw new InvalidEntityException();

            $commentsEntities =  $em->getRepository("MirrorMirrorSiteBundle:Comment")
                ->findByFileIdOrderByWhen( $entity->getId() );

            $comments = [];
            foreach($commentsEntities as $entityComment) {
                $newComment = new TComment($entityComment);
                $newComment->setUser(
                    $em->getRepository("MirrorMirrorSiteBundle:User")->find($entityComment->getUserId())
                );
                $comments[] = $newComment;
            }

            $UserVote = $em->getRepository("MirrorMirrorSiteBundle:Vote")
                ->findUserVoteValue($this->getUser(), $entity);

            $posts[] = new TPost($entity, $comments, $UserVote);
        }
        return $posts;
    }

    /**
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function homeAction($page = 0)
    {
       return $this->createPage($page, 3);
    }

    public function sortAction($page = 0, $sort)
    {
       return $this->createPage($page, 3, $sort);
    }

}
