<?php
namespace Mirror\MirrorSiteBundle\Controller;

use Mirror\MirrorSiteBundle\Entity\Attachment;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class FileController
 * @package Mirror\MirrorSiteBunde\Controller
 */
class FileController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function addAction(Request $request)
    {
        if(!$request->isXmlHttpRequest())
            throw $this->createNotFoundException();
        $file = new Attachment();

        $tmpFile = $request->files->get('file');
        if(!$tmpFile->isValid())
            throw $this->createAccessDeniedException();

        try {
            $file->setFile($tmpFile);
        } catch(\Exception $e) {
            throw $this->createAccessDeniedException();
        }

        $file->setName($request->request->get('name'));
        $file->setVote(0);
        $file->setIp(
            $this->container->get('request_stack')->getCurrentRequest()->getClientIp()
        );


        $em = $this->getDoctrine()->getManager();

        if( !$file->upload() )
            return new Response('Access denied.', 403);

        $em->persist($file);
        $em->flush();
        $response['url'] = 'http://' .
            $request->getHost() . $this->generateUrl('viewer', [ 'fileName' => $file->getPath() ]);
        return new JsonResponse($response);
    }
}
