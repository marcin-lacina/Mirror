<?php
namespace Mirror\MirrorSiteBundle\Controller;

use Mirror\MirrorSiteBundle\Entity\User;

use Mirror\MirrorSiteBundle\Form\Type\RegisterType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class SecurityController
 * @package Mirror\MirrorSiteBunde\Controller
 */
class SecurityController extends Controller
{

    /**
     * @param Request $request
     * @return Response
     */
    public function registerAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');
        $lastUserName = $authenticationUtils->getLastUsername();
        $authError = $authenticationUtils->getLastAuthenticationError();

        $user = new User();
        $form = $this->createForm(new RegisterType(), $user);
        $form->handleRequest($request);
        $isPost = $form->isValid();

        if( $isPost ) {
            if( !$user->encryptPass() ) $isPost = "Passwords are different.";
            else {
                $em = $this->getDoctrine()->getManager();
                $exist = $em->getRepository("MirrorMirrorSiteBundle:User")->findByUsername( $user->getUsername() );
                if(count($exist)) $isPost = "This user already exists. Choose a different login.";
                else {
                    $em->persist($user);
                    $em->flush();
                    $isPost = "Your account has been created. You can log on.";
                }
            }
        }
       return $this->render("MirrorMirrorSiteBundle:Security:register.html.twig", [
            'form' => $form->createView(),
            'isPost' => $isPost,
            'Auth' => [ 'lastUserName' => $lastUserName, 'error' => $authError ],
       ]);
    }

    public function loginCheckAction()
    {
        // this controller will not be executed,
        // as the route is handled by the Security system
    }
}