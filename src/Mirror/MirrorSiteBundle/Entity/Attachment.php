<?php
namespace Mirror\MirrorSiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Mirror\MirrorSiteBundle\Entity\AttachmentRepository")
 */
class Attachment
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $path;

    /**
     * @var \DateTime
     *@ORM\Column(type="datetime")
     */
    protected $when_;

    /**
     * @ORM\Column(type="string", length=20)
     */
    protected $ip;

    /**
     * @ORM\Column(type="integer")
     */
    protected $vote;

    /**
     * @Assert\File(maxSize="40000000")
     */
    private $file;

    /**
     * @return null|string
     */
    public function getAbsolutePath() {
		return null === $this->path
		? null
		: $this->getUploadRootDir().DIRECTORY_SEPARATOR.$this->path;
	}

    /**
     * @return null|string
     */
    public function getWebPath() {
		return null === $this->path
		? null
		: $this->getUploadDir().DIRECTORY_SEPARATOR.$this->path;
	}

    /**
     * @return string
     */
    protected function getUploadRootDir() {
		return __DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".
        DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."web".DIRECTORY_SEPARATOR.$this->getUploadDir();
	}

    /**
     * @return string
     */
    protected function getUploadDir() {
		return 'bundles'.DIRECTORY_SEPARATOR.'Mirror'.DIRECTORY_SEPARATOR.'uploads';
	}

    /**
     * @param UploadedFile $file
     * @return UploadedFile
     */
    public function setFile(UploadedFile $file) {
        $this->file = $file;
        return $this->file;
    }

    /**
     * @return mixed
     */
    public function getFile() {
        return $this->file;
    }

    /**
     * @return bool
     */
    public function upload()
    {
        $fileStringName = $this->getFile()->getClientOriginalName();
        $fileName = substr($fileStringName, 0, strrpos($fileStringName, '.'));
        $extension = substr($fileStringName, strrpos($fileStringName, '.') + 1);

        $finalSaveName = uniqid().".".$extension;
        $this->path = $finalSaveName;


        $allow = ['jpg', 'png', 'gif', 'mp4', 'ogg'];

        // the file property can be empty if the field is not required
        if (
            null === $this->getFile() ||
            !in_array($extension, $allow) ||
            !strlen($fileName) ||
            !strlen($extension)
        ) {
            return false;
        }

        $this->getFile()->move(
            $this->getUploadRootDir(),
            $finalSaveName
        );

        // set the path property to the filename where you've saved the file

        $this->when_ = new \DateTime();

        // clean up the file property as you won't need it anymore
        $this->file = null;
        return true;
    }

    /**
     * @return bool
     */
    public function rmFromDisk()
    {
        return unlink($this->getAbsolutePath());
    }

    public function getVote() { return $this->vote; }
    /**
     * @param $vote
     * @return int
     */
    public function setVote($vote)
    {
        $this->vote = $vote;
        return $this->vote;
    }

    public function getIp() { return $this->ip; }
    /**
     * @param $ip_address
     * @return bool|string
     */
    public function setIp($ip_address) {
        if( empty($ip_address) ) {
            $this->ip = "Empty addr"; return false;
        }
        $this->ip = $ip_address;
        return $this->ip;
    }

    public function getName() { return $this->name; }
    /**
     * @param $name
     * @return mixed
     */
    public function setName($name) {
        $this->name = $name;
        return $this->name;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
        if( strlen($this->name) > 20 ) return substr($this->name, 0, 19)."..";
        return $this->name;
    }

    public function getPath() { return $this->path; }
    /**
     * @param $path
     * @return mixed
     */
    public function setPath($path) {
        $this->path=$path;
        return $this->path;
    }

    /**
     * @return \DateTime
     */
    public function getWhen_() { return $this->when_; }
    /**
     * @param $when
     * @return mixed
     */
    public function setWhen_($when) {
        $this->when_ = $when;
        return $this->when_;
    }

    public function getId() { return $this->id; }
    /**
     * @param $id
     * @return mixed
     */
    public function setId($id) {
        $this->id = $id;
        return $this->id;
    }

    /**
     * @return string
     */
    public function getExtension() {
        return substr($this->path, strrpos($this->path, '.') + 1);
    }

    /**
     * @return bool
     */
    public function isVideo()
    {
        if( in_array($this->getExtension(), ['mp4', 'ogg']) ) return true;
        return false;
    }

    /**
     * @return bool|string
     */
    public function getVideoType()
    {
        if($this->isVideo()) return "video/".$this->getExtension();
        return false;
    }

    /**
     * @param $upOrDown
     * @return int
     */
    public function voting($upOrDown)
    {
        $upOrDown == 1 ? $this->vote++ : $this->vote--;
        return $this->vote;
    }


}
