<?php
namespace Mirror\MirrorSiteBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class RegisterType
 * @package Mirror\MirrorSiteBundle\Form\Type
 */
class RegisterType extends AbstractType
{
    /**
     * @return string
     */
    public function getName()
    {
        return "register_form";
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'text', [
                'label' => 'Username:'
            ])
            ->add('password', 'password', [
                'label' => 'Password: '
            ])
            ->add('returnPassword', 'password', [
                'label' => 'Return password: '
            ])
            ->add('submit', 'submit', [
                'label' => 'Create account'
            ]);
    }
}