#!/bin/sh
if [ `pwd` = '/var/mirror' ] ; then
    echo "You are crazy..";
else

    if [ -d "/var/mirror/web/bundles/Mirror/uploads" ] ; then

        if [ -d "./../web/bundles/Mirror/uploads/" ] ; then
            rm -rf "./../web/bundles/Mirror/uploads/";
            echo "Deleted existing upload directory.";
        fi
        cd ./../web/bundles/Mirror
        ln -s "/var/mirror/web/bundles/Mirror/uploads" uploads
        echo "Created sym-link to official Mirror upload directory";
        cd -

    else
        echo "Main uploads folder not exist in this system."
    fi

fi