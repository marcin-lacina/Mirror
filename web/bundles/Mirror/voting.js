var voteElements = document.getElementsByClassName('voting');

for(var i=0; i<voteElements.length; i++) {
    voteElements[i].addEventListener('click', function() {
        var voteInt = -1;
        if(this.getAttribute('Title') == 'Upvote') voteInt = 1;
        var ajax = new XMLHttpRequest();
        var params = 'file='+this.parentNode.getAttribute('id').replace('fileVote','')+'&vote='+voteInt;
        ajax.open('POST', '/user-ajax-vote', true);
        ajax.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        ajax.setRequestHeader('Content-type', "application/x-www-form-urlencoded");
        ajax.onreadystatechange = function() {
            if(ajax.readyState == 4) {
                if(ajax.status == 200) {
                    var response = JSON.parse(ajax.responseText);
                    var voteElement = document.getElementById('fileVote'+response.fileVote).getElementsByTagName('span')[0];
                    voteElement.innerHTML = response.value;
                    var votesUpDown = document.getElementById('fileVote'+response.fileVote).getElementsByTagName('i');
                    votesUpDown[0].style.color = response.colors.up;
                    votesUpDown[1].style.color = response.colors.down;
                }
            }
        };
        ajax.send(params);
    });
}