'use strict';
var nav = {
    sticky: false,
    elem: document.getElementsByTagName('nav')[0],
    headerHeight: document.getElementsByTagName('header')[0].offsetHeight,
    scroll: function() {
        var higher = document.body.scrollTop > nav.headerHeight - nav.elem.offsetHeight;
        if (higher && !nav.sticky) {
            nav.elem.style.position = 'fixed';
            document.body.style.marginTop = '' + nav.elem.offsetHeight + 'px';
            nav.sticky = true;
        } else if (!higher && nav.sticky) {
            nav.elem.style.position = 'relative';
            document.body.style.marginTop = 0;
            nav.sticky = false;
        }
    }
}, blockConts = {
    elem: [].slice.call(document.getElementsByClassName('block')),
    clickToggle: function(elem) {
        elem.addEventListener('click', function (e) {
            var comments = e.target.parentNode.parentNode.getElementsByClassName('commentCont')[0];
            blockConts.commentsToggle(comments);
        });
    },
    commentsToggle: function(e) {
        var elem = e.parentNode.getElementsByClassName('block')[0].style;
        if (!e.style.width || e.style.width == '0px') {
            e.style.width = '0';
            elem.maxWidth = '70%';

            setTimeout(function () {
                return e.style.width = '30%';
            }, 0);
        } else {
            e.style.width = '0';
            elem.maxWidth = '100%';
        }
    },
    scroll: function() {
        blockConts.elem.forEach(function (elem) {
            var pos = parseInt(elem.getBoundingClientRect().top), body = { height: getComputedStyle(document.body).height.split('px')[0] };
            console.log(pos, body.height);
            if (pos * 2 < 50) {
                elem.parentNode.style.maxWidth = '75%';
            } else if (pos * 2 < body.height / 2) {
                elem.parentNode.style.maxWidth = '100%';
            } else {
                elem.parentNode.style.maxWidth = '75%';
            }
        });
    }
}, addNew = {
    clicked: false,
    elem: document.getElementById('addNew'),
    form: document.getElementsByClassName('fileForm'),

    show: function() {
        if (!addNew.clicked) {
            addNew.clicked = true;
            addNew.css({
                'width': '20em',
                'height': '12em',
                'border-radius': 0
            });
            addNew.elem.getElementsByTagName('div')[0].style.display = 'none';
            for(var i=0; i<addNew.form.length; i++) {
                addNew.form[i].style.display = 'block';
            }
        }
    },
    hide: function() {
        if(addNew.clicked) {
            addNew.css({
                'width': '4em',
                'height': '4em',
                'border-radius': '50%'
            });
            for(var i=0; i<addNew.form.length; i++) {
                addNew.form[i].style.display = 'none';
            }
            setTimeout(function () {
                addNew.elem.getElementsByTagName('div')[0].style.display = 'block';
                addNew.clicked = false;
            }, 500);
        }
    },
    css: function(_css) {
        for (var key in _css) {
            addNew.elem.style[key] = _css[key];
        }
    }
};

var comments = document.getElementsByClassName('comments');
for(var i=0; i<comments.length; i++) {
    comments[i].scrollTop = comments[i].scrollHeight;
}

document.getElementById('fileSend').onclick = function() {
    if(!document.getElementById('fileTitle').value.length) {
        alert("File name required."); return false;
    }
    var ajaxFormData = new FormData(
        document.getElementById('fileFormBlock').getElementsByTagName('form')[0]
    );
    var ajax = new XMLHttpRequest();
    ajax.open('POST', '/add', true);
    ajax.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    ajax.onreadystatechange = function() {
        if(ajax.readyState == 4) {
            if(ajax.status == 200) {
                var serverResponse = JSON.parse(ajax.responseText);
                prompt('File upload ok. Copy your unique link.', serverResponse.url);
            } else
            alert('Fatal error when uploading your file.\nCheck extension and file size.');
        }
    };
    ajax.send(ajaxFormData);
};

document.getElementById('addNew').addEventListener('click', addNew.show);
document.getElementById('xBlock').addEventListener('click', addNew.hide);

document.addEventListener('scroll', nav.scroll);
blockConts.elem.forEach(blockConts.clickToggle);