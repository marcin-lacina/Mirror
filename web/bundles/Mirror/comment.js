var inputs = document.getElementsByClassName('newComments');

for(var i=0; i<inputs.length; i++) {
    inputs[i].addEventListener('keydown', function(e) {
        if(e.keyCode == 13 && !e.shiftKey) {
            this.setAttribute('disabled', 'true');
            var fileId = this.getAttribute('id').replace('newComment', '');
            var message = this.value;
            var ajax = new XMLHttpRequest();
            var params = 'file='+fileId+'&comment='+message;
            ajax.open('POST', '/user-ajax-comment', true);
            ajax.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            ajax.setRequestHeader('Content-type', "application/x-www-form-urlencoded");

            ajax.onreadystatechange = function() {
                if (ajax.readyState == 4) {
                    if(ajax.status == 200) {
                        var response = JSON.parse(ajax.responseText);
                        var newListItem = document.createElement('li');
                        var dateTimeSpan = document.createElement('span');
                        var head5 = document.createElement('h5');
                        var commentMessDiv = document.createElement('div');
                        dateTimeSpan.innerHTML = response.when;
                        head5.innerHTML = response.userName;
                        head5.appendChild(dateTimeSpan);
                        commentMessDiv.innerHTML = message;
                        newListItem.appendChild(head5);
                        newListItem.appendChild(commentMessDiv);
                        var textarea = document.getElementById('newComment' + fileId);
                        textarea.parentNode.parentNode.insertBefore(newListItem, textarea.parentNode);
                        textarea.value = '';
                        textarea.removeAttribute('disabled');
                        textarea.parentNode.parentNode.scrollTop = textarea.parentNode.parentNode.scrollHeight;
                    } else alert("Failed to add your comment");
                }
            };
            ajax.send(params);
        }
    });
}